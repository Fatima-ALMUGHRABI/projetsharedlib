package tp1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectDevOps21Application {

	public static void main(String[] args) {
		SpringApplication.run(ProjectDevOps21Application.class, args);
		System.out.println("I am running!");
	}

}
