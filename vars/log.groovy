def call(){
    pipeline {
        agent any
        tools {
            maven 'M3' 
        }
        stages {
            stage('Clean') {
                steps {
                    sh "mvn clean"
                }
            }
            stage('Checkstyle') {
                steps {
                    catchError(buildResult:'SUCCESS' , stageResult : 'FAILURE'){//UNSTABLE
                        sh "mvn checkstyle:check"
                    }
                }
            }
            stage('Code Coverage') {
                steps {
                    sh "mvn clean cobertura:cobertura"
                }
            }
            stage('Build'){
                steps{
                    sh "mvn verify"
                }
            }
            stage('Compile'){
                steps{
                    script {
                        if(env.BRANCH_NAME == "main"){
                            sh 'mvn -Pprod package'
                        }
                        if(env.BRANCH_NAME == "develop"){
                            sh 'mvn -Pdev package'
                        }
                    }
                }
            }
            stage('Deploy') {
                steps{
                    script {
                        if(env.BRANCH_NAME == "main"){
                            withCredentials([usernamePassword(credentialsId: 'heroku-api-key-falmughrabi', passwordVariable: 'fe875af4-5d3d-40ad-b25d-4ad30cf0569c', usernameVariable: '')]){
                                sh 'git push --force https://heroku:fe875af4-5d3d-40ad-b25d-4ad30cf0569c@git.heroku.com/m2iceld-myapp-fm.git HEAD:refs/heads/main';
                            }
                        }
                        if(env.BRANCH_NAME == "develop"){
                           withCredentials([usernamePassword(credentialsId: 'heroku-api-key-falmughrabi', passwordVariable: 'fe875af4-5d3d-40ad-b25d-4ad30cf0569c', usernameVariable: '')]){
                                sh 'git push --force https://heroku:fe875af4-5d3d-40ad-b25d-4ad30cf0569c@git.heroku.com/m2iceld-myapp-fm-staging.git HEAD:refs/heads/main';
                            }
                        }
                    }
                }
            }
        }
    }
}